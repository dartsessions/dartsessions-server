var useSecure = false;
var maxUsers = 4;

var existingRooms = {};
var rejectedUsers = [];

// 'room1' : { isLocked: false, isPublic: false, isClaimed: false, adminUser: easyrtcid, numUsers: 0, activeUserIDs: {}}; // our room object template


// Load required modules
var http 	= require("http");
var https   = require("https");              // http server core module
var fs		= require("fs");
var express = require("express");           // web framework external module
var io      = require("socket.io");         // web socket external module
var easyrtc = require("easyrtc");           // EasyRTC external module
var qs 		= require('querystring');
var util 	= require('util');
var nodemailer = require("nodemailer");


// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "dartsessions@gmail.com",
        pass: "webRTC2014!"
    }
});

// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var httpApp = express();

httpApp.use(express.static(__dirname + "/static/"));



if (useSecure) {
	// Start Express https server on port 8080
	var webServer = https.createServer(
	{
		
	//john
		key: fs.readFileSync("keys/86941803-27.147.203.130_8080.key"),
		cert: fs.readFileSync("keys/86941803-27.147.203.130_8080.cert")
	
		
	//ifte
	//	key: fs.readFileSync("keys/80714008-27.147.180.252_8080_.key"),
	//	cert: fs.readFileSync("keys/80714008-27.147.180.252_8080_.cert")	
		
	},httpApp).listen(8080);
	
} else {	
	var webServer = http.createServer(httpApp).listen(8080);
}

// Start Socket.io so it attaches itself to Express server

var socketServer = io.listen(webServer, {"log level":1});

// Ice Config Start
var myIceConfig = [
	{url: "stun:stun.l.google.com:19302"},
	{url: "stun:stun.sipgate.net"},
	{url: "stun:217.10.68.152"},
	{url: "stun:stun.sipgate.net:10000"},
	{url: "stun:217.10.68.152:10000"},
	{url: "turn:numb.viagenie.ca", username: "dartsessions@gmail.com", credential: "dartNumbStunTurn"}
//	{url: "turn:27.147.187.35", username: "himadree", credential: "youhavetoberealistic"}
];

easyrtc.setOption("appIceServers", myIceConfig);
easyrtc.setOption("roomDefaultEnable", false);

// Ice Config End



//************* Custom Functions ************* 

function SHA256(s){
 
    var chrsz   = 8;
    var hexcase = 0;
 
    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }
 
    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
    function R (X, n) { return ( X >>> n ); }
    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }
 
    function core_sha256 (m, l) {
        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;
 
        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;
 
        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];
 
            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
 
                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));
 
                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }
 
            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }
 
    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }
 
    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
 
        for (var n = 0; n < string.length; n++) {
 
            var c = string.charCodeAt(n);
 
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
 
        }
 
        return utftext;
    }
 
    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
        }
        return str;
    }
 
    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
 
}


function date (format, timestamp) { // http://phpjs.org/functions/date/
  // From: http://phpjs.org/functions
  // +   original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
  // +      parts by: Peter-Paul Koch (http://www.quirksmode.org/js/beat.html)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: MeEtc (http://yass.meetcweb.com)
  // +   improved by: Brad Touesnard
  // +   improved by: Tim Wiel
  // +   improved by: Bryan Elliott
  // +   improved by: David Randall
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Theriault
  // +  derived from: gettimeofday
  // +      input by: majak
  // +   bugfixed by: majak
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +      input by: Alex
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +   improved by: Thomas Beaucourt (http://www.webapp.fr)
  // +   improved by: JT
  // +   improved by: Theriault
  // +   improved by: Rafał Kukawski (http://blog.kukawski.pl)
  // +   bugfixed by: omid (http://phpjs.org/functions/380:380#comment_137122)
  // +      input by: Martin
  // +      input by: Alex Wilson
  // +      input by: Haravikk
  // +   improved by: Theriault
  // +   bugfixed by: Chris (http://www.devotis.nl/)
  // %        note 1: Uses global: php_js to store the default timezone
  // %        note 2: Although the function potentially allows timezone info (see notes), it currently does not set
  // %        note 2: per a timezone specified by date_default_timezone_set(). Implementers might use
  // %        note 2: this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST set by that function
  // %        note 2: in order to adjust the dates in this function (or our other date functions!) accordingly
  // *     example 1: date('H:m:s \\m \\i\\s \\m\\o\\n\\t\\h', 1062402400);
  // *     returns 1: '09:09:40 m is month'
  // *     example 2: date('F j, Y, g:i a', 1062462400);
  // *     returns 2: 'September 2, 2003, 2:26 am'
  // *     example 3: date('Y W o', 1062462400);
  // *     returns 3: '2003 36 2003'
  // *     example 4: x = date('Y m d', (new Date()).getTime()/1000);
  // *     example 4: (x+'').length == 10 // 2009 01 09
  // *     returns 4: true
  // *     example 5: date('W', 1104534000);
  // *     returns 5: '53'
  // *     example 6: date('B t', 1104534000);
  // *     returns 6: '999 31'
  // *     example 7: date('W U', 1293750000.82); // 2010-12-31
  // *     returns 7: '52 1293750000'
  // *     example 8: date('W', 1293836400); // 2011-01-01
  // *     returns 8: '52'
  // *     example 9: date('W Y-m-d', 1293974054); // 2011-01-02
  // *     returns 9: '52 2011-01-02'
    var that = this,
      jsdate,
      f,
      // Keep this here (works, but for code commented-out
      // below for file size reasons)
      //, tal= [],
      txt_words = ["Sun", "Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      // trailing backslash -> (dropped)
      // a backslash followed by any character (including backslash) -> the character
      // empty string -> empty string
      formatChr = /\\?(.?)/gi,
      formatChrCb = function (t, s) {
        return f[t] ? f[t]() : s;
      },
      _pad = function (n, c) {
        n = String(n);
        while (n.length < c) {
          n = '0' + n;
        }
        return n;
      };
  f = {
    // Day
    d: function () { // Day of month w/leading 0; 01..31
      return _pad(f.j(), 2);
    },
    D: function () { // Shorthand day name; Mon...Sun
      return f.l().slice(0, 3);
    },
    j: function () { // Day of month; 1..31
      return jsdate.getDate();
    },
    l: function () { // Full day name; Monday...Sunday
      return txt_words[f.w()] + 'day';
    },
    N: function () { // ISO-8601 day of week; 1[Mon]..7[Sun]
      return f.w() || 7;
    },
    S: function(){ // Ordinal suffix for day of month; st, nd, rd, th
      var j = f.j(),
        i = j%10;
      if (i <= 3 && parseInt((j%100)/10, 10) == 1) {
        i = 0;
      }
      return ['st', 'nd', 'rd'][i - 1] || 'th';
    },
    w: function () { // Day of week; 0[Sun]..6[Sat]
      return jsdate.getDay();
    },
    z: function () { // Day of year; 0..365
      var a = new Date(f.Y(), f.n() - 1, f.j()),
        b = new Date(f.Y(), 0, 1);
      return Math.round((a - b) / 864e5);
    },

    // Week
    W: function () { // ISO-8601 week number
      var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3),
        b = new Date(a.getFullYear(), 0, 4);
      return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
    },

    // Month
    F: function () { // Full month name; January...December
      return txt_words[6 + f.n()];
    },
    m: function () { // Month w/leading 0; 01...12
      return _pad(f.n(), 2);
    },
    M: function () { // Shorthand month name; Jan...Dec
      return f.F().slice(0, 3);
    },
    n: function () { // Month; 1...12
      return jsdate.getMonth() + 1;
    },
    t: function () { // Days in month; 28...31
      return (new Date(f.Y(), f.n(), 0)).getDate();
    },

    // Year
    L: function () { // Is leap year?; 0 or 1
      var j = f.Y();
      return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
    },
    o: function () { // ISO-8601 year
      var n = f.n(),
        W = f.W(),
        Y = f.Y();
      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
    },
    Y: function () { // Full year; e.g. 1980...2010
      return jsdate.getFullYear();
    },
    y: function () { // Last two digits of year; 00...99
      return f.Y().toString().slice(-2);
    },

    // Time
    a: function () { // am or pm
      return jsdate.getHours() > 11 ? "pm" : "am";
    },
    A: function () { // AM or PM
      return f.a().toUpperCase();
    },
    B: function () { // Swatch Internet time; 000..999
      var H = jsdate.getUTCHours() * 36e2,
        // Hours
        i = jsdate.getUTCMinutes() * 60,
        // Minutes
        s = jsdate.getUTCSeconds(); // Seconds
      return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
    },
    g: function () { // 12-Hours; 1..12
      return f.G() % 12 || 12;
    },
    G: function () { // 24-Hours; 0..23
      return jsdate.getHours();
    },
    h: function () { // 12-Hours w/leading 0; 01..12
      return _pad(f.g(), 2);
    },
    H: function () { // 24-Hours w/leading 0; 00..23
      return _pad(f.G(), 2);
    },
    i: function () { // Minutes w/leading 0; 00..59
      return _pad(jsdate.getMinutes(), 2);
    },
    s: function () { // Seconds w/leading 0; 00..59
      return _pad(jsdate.getSeconds(), 2);
    },
    u: function () { // Microseconds; 000000-999000
      return _pad(jsdate.getMilliseconds() * 1000, 6);
    },

    // Timezone
    e: function () { // Timezone identifier; e.g. Atlantic/Azores, ...
      // The following works, but requires inclusion of the very large
      // timezone_abbreviations_list() function.
/*              return that.date_default_timezone_get();
*/
      throw 'Not supported (see source code of date() for timezone on how to add support)';
    },
    I: function () { // DST observed?; 0 or 1
      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
      // If they are not equal, then DST is observed.
      var a = new Date(f.Y(), 0),
        // Jan 1
        c = Date.UTC(f.Y(), 0),
        // Jan 1 UTC
        b = new Date(f.Y(), 6),
        // Jul 1
        d = Date.UTC(f.Y(), 6); // Jul 1 UTC
      return ((a - c) !== (b - d)) ? 1 : 0;
    },
    O: function () { // Difference to GMT in hour format; e.g. +0200
      var tzo = jsdate.getTimezoneOffset(),
        a = Math.abs(tzo);
      return (tzo > 0 ? "-" : "+") + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
    },
    P: function () { // Difference to GMT w/colon; e.g. +02:00
      var O = f.O();
      return (O.substr(0, 3) + ":" + O.substr(3, 2));
    },
    T: function () { // Timezone abbreviation; e.g. EST, MDT, ...
      // The following works, but requires inclusion of the very
      // large timezone_abbreviations_list() function.
/*              var abbr = '', i = 0, os = 0, default = 0;
      if (!tal.length) {
        tal = that.timezone_abbreviations_list();
      }
      if (that.php_js && that.php_js.default_timezone) {
        default = that.php_js.default_timezone;
        for (abbr in tal) {
          for (i=0; i < tal[abbr].length; i++) {
            if (tal[abbr][i].timezone_id === default) {
              return abbr.toUpperCase();
            }
          }
        }
      }
      for (abbr in tal) {
        for (i = 0; i < tal[abbr].length; i++) {
          os = -jsdate.getTimezoneOffset() * 60;
          if (tal[abbr][i].offset === os) {
            return abbr.toUpperCase();
          }
        }
      }
*/
      return 'UTC';
    },
    Z: function () { // Timezone offset in seconds (-43200...50400)
      return -jsdate.getTimezoneOffset() * 60;
    },

    // Full Date/Time
    c: function () { // ISO-8601 date.
      return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
    },
    r: function () { // RFC 2822
      return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
    },
    U: function () { // Seconds since UNIX epoch
      return jsdate / 1000 | 0;
    }
  };
  this.date = function (format, timestamp) {
    that = this;
    jsdate = (timestamp === undefined ? new Date() : // Not provided
      (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
      new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
    );
    return format.replace(formatChr, formatChrCb);
  };
  return this.date(format, timestamp);
}




var replaceAll = function(str, str1, str2, ignore) 
{
   return str.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};

var consoleLogger = function(msg){
	console.log("");	
	console.log(msg);
}

var logger = function(msg){
		var logData = new Date().getTime() + " | " + msg + "\r\n";
		fs.appendFile('log.txt', logData, function (err) {
			if (err) throw err;
		});		
}

var emailLogger = function(msg){
		var logData = new Date().getTime() + " | " + msg + "\r\n";
		fs.appendFile('email.txt', logData, function (err) {
			if (err) throw err;
		});		
}
var findOldestRoomUser = function(roomName){
	if(typeof existingRooms[roomName] == 'undefined') return 'null';
	var id = '';
	for(var i in existingRooms[roomName].activeUserIDs){
		if(id === ''){
			id = i;
		} else {
			if(existingRooms[roomName].activeUserIDs[i] < existingRooms[roomName].activeUserIDs[id]){
				id = i;
			}
		}

	}
	return id;
};

// on Shutdown

var customListenerForShutdown = function(next){
	
	var afterShutdown = function(err){
		if (err) { 
			logger(err);
			next(err);
			return;
		}
		
		var logMessage = "Server shut down";
		consoleLogger(logMessage);
		logger(logMessage);

		next(null);
	}

	easyrtc.events.emitDefault("shutdown", afterShutdown);	
	
};
easyrtc.events.on("shutdown", customListenerForShutdown);	




// onStartup

var customListenerForStartup = function(next){
	
	var afterStartup = function(err){
		if (err) { 
			logger(err);
			next(err);
			return;
		}
		
		var logMessage = "Server started";
		consoleLogger(logMessage);
		logger(logMessage);
		

		next(null);
	}

	easyrtc.events.emitDefault("startup", afterStartup);	
	
};
easyrtc.events.on("startup", customListenerForStartup);	
























/*
// Get iceConfig from Anyfirewall.com
// Custom listener for "getIceConfig" to request a custom iceConfig for every request
easyrtc.on("getIceConfig", function(connectionObj, callback){
	
	var easyrtcid = connectionObj.getEasyrtcid();
	
	consoleLogger(easyrtcid + " requested ice config!");

	var request = require('request');

	var phpTime = Math.floor(new Date().getTime() / 1000); // equivalent to php time()
	
	var phpDate = date("Y-m-d", phpTime); // equivalent to php date("Y-m-d", time())

	hash = SHA256("webRTC2014!_" + phpDate);
		
	finalUrl = "http://turn.anyfirewall.com/ephemeral-credential.php?service=TURN&username=Dartsessions&key=" + hash;

	request.post(
		finalUrl,
		[],
		function (error, response, body) {
			if (!error && response.statusCode == 200) {
				
//				var iceConfig = [{url: "stun:stun.l.google.com:19302"}, {url: "stun:stun.anyfirewall.com:3478"}]; // both google and anyfirewall
				var iceConfig = [ {url: "stun:stun.anyfirewall.com:3478"}];
											
				var tsPacket = JSON.parse(body);

				for (var i = 0; i < tsPacket.uris.length; i++) {
				  iceConfig.push({
					"url":tsPacket.uris[i],
					"username":tsPacket.username,
					"credential":tsPacket.password
				  });
				}
		
				consoleLogger("Successfully sent iceConfig with Anyfirewall.com Turn Servers to user " + easyrtcid);
				callback(null, iceConfig);
				
			}
		}
	);	

});

*/





















			
			
			
			


// Get iceConfig from xirsys.com
// Custom listener for "getIceConfig" to request a custom iceConfig for every request
easyrtc.on("getIceConfig", function(connectionObj, callback){
	
	var easyrtcid = connectionObj.getEasyrtcid();
	
	consoleLogger(easyrtcid + " requested ice config!");

		
	finalUrl = "https://api.xirsys.com/getIceServers";
	
	var needle = require('needle');
	
	needle.post('https://api.xirsys.com/getIceServers', 
	{ domain: "www.dartsessions.com", room: "default", application: "default", ident: "dartsessions", secret: "365ea801-b06a-4013-bd00-0f9bb6d9376b", secure: 1 }, 
	function(err, resp, body) {
		var tsPacket = JSON.parse(body);
		console.log(tsPacket.d.iceServers);
		
		
		var iceConfig = tsPacket.d.iceServers ;

		consoleLogger("Successfully sent iceConfig with xirsys.com Turn Servers to user " + easyrtcid);
		callback(null, iceConfig);		
		
	});	
	


});


			


/*
// Get iceConfig from turnservers.com
// Custom listener for "getIceConfig" to request a custom iceConfig for every request
easyrtc.on("getIceConfig", function(connectionObj, callback){
	
	var easyrtcid = connectionObj.getEasyrtcid();
	
	consoleLogger(easyrtcid + " requested ice config!");

	http.get("http://api.turnservers.com/json/turn?key=WyJDoJErszXTajVwWlIxutoMDPwWyokp", function(res){
		var data = '';
	
		res.on('data', function (chunk){
			data += chunk;
		});
	
		res.on('end',function(){

			var tsPacket = JSON.parse(data);			

			// TODO: Proper error handling to ensure request succeeded.

//			var iceConfig = [{url:"stun.turnservers.com:3478"}];
			var iceConfig = [{url: "stun:stun.l.google.com:19302"}];
			

			
			for (var i = 0; i < tsPacket.uris.length; i++) {
			  iceConfig.push({
				"url":tsPacket.uris[i],
				"username":tsPacket.username,
				"credential":tsPacket.password
			  });
			}
			
			iceConfig.push({
				"url":"turn:numb.viagenie.ca",
				"username":"dartsessions@gmail.com",
				"credential":"dartNumbStunTurn"
			});
			
	
			consoleLogger("Successfully sent iceConfig with turnservers.com Turn Servers to user " + easyrtcid);
			callback(null, iceConfig);
			
		})
	
	});

});
*/








/*

// Get iceConfig from tawk.com
// Custom listener for "getIceConfig" to request a custom iceConfig for every request
easyrtc.on("getIceConfig", function(connectionObj, callback){
	
	var easyrtcid = connectionObj.getEasyrtcid();
	
	consoleLogger(easyrtcid + " requested ice config!");


	var iceConfig = [
						{url: "stun:stun.sipgate.net"},
						{url: "stun:217.10.68.152"},
						{url: "stun:stun.sipgate.net:10000"},
						{url: "stun:217.10.68.152:10000"},
						{url: "turn:192.155.84.88", username: "easyRTC", credential: "easyRTC@pass"},
						{url: "turn:192.155.84.88?transport=tcp", username: "easyRTC", credential: "easyRTC@pass"},						
						{url: "turn:192.155.86.24:443", username: "easyRTC", credential: "easyRTC@pass"},	
						{url: "turn:192.155.86.24:443?transport=tcp", username: "easyRTC", credential: "easyRTC@pass"}						
					];

	callback(null, iceConfig);

});

*/











// Connection Making Start

var customListenerForConnection = function(socket, easyrtcid, next){
	
	var afterConnection = function(err){
		if (err) { 
			logger(err);
			next(err);
			return;
		}
		
		var address = socket.handshake.address;

		var logMessage = "--- New connection from " + address.address + ":" + address.port + " | ID: " + easyrtcid;
		consoleLogger(logMessage);
		logger(logMessage);
	

		next(null);
	}

	easyrtc.events.emitDefault("connection", socket, easyrtcid, afterConnection);	
	
};
easyrtc.events.on("connection", customListenerForConnection);	
// Connection Making End



var customListenerForAuthenticate = function(socket, easyrtcid, appName, username, credential, easyrtcAuthMessage, next){
	
	var afterAuthenticate = function(err){
		if (err) { 
			logger(err);
			next(err);
			return;
		}
		
		var logMessage = "Authentication request from " + easyrtcid + " | username: " + username;
		consoleLogger(logMessage);
		logger(logMessage);
	

		next(null);
	}

	easyrtc.events.emitDefault("authenticate", socket, easyrtcid, appName, username, credential, easyrtcAuthMessage, afterAuthenticate);	
	
};
easyrtc.events.on("authenticate", customListenerForAuthenticate);	



var customListenerForAuthenticated = function(connectionObj, next){
	
	var easyrtcid = connectionObj.getEasyrtcid();	
	
	var afterAuthenticated = function(err){
		if (err) { 
			logger(err);
			next(err);
			return;
		}
		
		var logMessage = "Authenticated: " + easyrtcid + " | username: " + connectionObj.getUsername();
		consoleLogger(logMessage);
		logger(logMessage);
	

		next(null);
	}

	easyrtc.events.emitDefault("authenticated", connectionObj, afterAuthenticated);	
	
};
easyrtc.events.on("authenticated", customListenerForAuthenticated);	




// Room Joininig Start
var customListenerForRoomJoin = function(connectionObj, roomName, callback){
	
	var afterRoomJoin = function(err){

		if (err) { 
			logger(err);
			callback(err);
			return;
		}		
	
		var easyrtcid = connectionObj.getEasyrtcid();
		
		var joinMessage = "You just joined " + roomName;
		
		var logMessage = easyrtcid + " joined room " + roomName;
		consoleLogger(logMessage);
		logger(logMessage);		
		
		if (existingRooms[roomName]){ // room exists
			consoleLogger("Room Already Exists");
		} else {
			
			var logMessage = roomName + " just Created";
			consoleLogger(logMessage);
			logger(logMessage);			
			
			existingRooms[roomName] = { isLocked: false, isPublic: false, isClaimed: false, adminUser: easyrtcid, numUsers: 0, activeUserIDs: {}}; // our room object template
		}
		
		// Send a message Start
		if (existingRooms[roomName]){
			if(existingRooms[roomName].isLocked){
				easyrtc.events.emit("emitEasyrtcMsg", connectionObj, "roomLocked", {msgData:joinMessage}, "socket reponse - success", function(){
					rejectedUsers.push(easyrtcid);
					consoleLogger("Message sent to " + easyrtcid + " in room " + roomName + " telling user that room is locked");
				});
			} else if(existingRooms[roomName].numUsers==maxUsers){
				easyrtc.events.emit("emitEasyrtcMsg", connectionObj, "roomFull", {msgData:joinMessage}, "socket reponse - success", function(){
					rejectedUsers.push(easyrtcid);
					consoleLogger("Message sent to " + easyrtcid + " in room " + roomName + "telling him that room is full");
				});						
			} else {
				easyrtc.events.emit("emitEasyrtcMsg", connectionObj, "successMessage", {msgData:joinMessage}, "socket reponse - success", function(){
						existingRooms[roomName].numUsers++;  //updates number of users on successful join
						existingRooms[roomName].activeUserIDs[easyrtcid] = new Date().getTime();
						consoleLogger("Message [success] sent to " + easyrtcid + " in room " + roomName + " with " + existingRooms[roomName].numUsers + " users");						
				});				
			}
		}
		// Send a message End	
		callback(null);
	}
	
	easyrtc.events.emitDefault("roomJoin", connectionObj, roomName, afterRoomJoin);
	
};
easyrtc.events.on("roomJoin", customListenerForRoomJoin);
// Room Joininig End	


// Room Leave Start
var customListenerForRoomLeave = function(connectionObj, roomName, next){


	// This gets run after a connection has left a room
	var afterRoomLeave = function(err) {
		
		if (err) { 
			logger(err);
			next(err);
			return;
		}		

		var id = connectionObj.getEasyrtcid();
		
		if(rejectedUsers.indexOf(id)>-1){
			rejectedUsers.splice(rejectedUsers.indexOf(id),1);
		} else {
			existingRooms[roomName].numUsers--;						
			delete existingRooms[roomName].activeUserIDs[id];
		}
		
		var logMessage = id + " left " + roomName;
		consoleLogger(logMessage);
		logger(logMessage);
		
		
		
		if (existingRooms[roomName].adminUser == id){
			consoleLogger("Admin User Just Left Room: " + roomName);	

			if(existingRooms[roomName].numUsers > 1){ //admin transfer

				var nextAdmin = findOldestRoomUser(roomName);
				if(nextAdmin !== 'null'){
					connectionObj.getApp().connection(nextAdmin, function(err, emitToConnectionObj){
						if (err) { 
							logger(err);
							next(err);
							return;
						} else {
							existingRooms[roomName].adminUser = nextAdmin;
							easyrtc.events.emit("emitEasyrtcMsg", emitToConnectionObj, "alphaMessage", {msgData:
																											{
																												state: "on",
																												isPublic: existingRooms[roomName].isPublic,
																												isLocked: existingRooms[roomName].isLocked
																											}
																											}, "socket reponse - success", function(){
													consoleLogger("Alpha Message sent to " + nextAdmin + " in room " + roomName);
												});
						}
					});					
				}
			} else if (existingRooms[roomName].numUsers == 1){
					var lastUser = findOldestRoomUser(roomName);
					
					existingRooms[roomName].adminUser = lastUser;
					connectionObj.getApp().connection(lastUser, function(err, emitToConnectionObj){
						if (err) { 
							logger(err);
							next(err);
							return;
						} else {
							easyrtc.events.emit("emitEasyrtcMsg", emitToConnectionObj, "alphaMessage", {msgData:
																											{	
																												state: "on",
																												isPublic: existingRooms[roomName].isPublic,
																												isLocked: existingRooms[roomName].isLocked
																											}
																										}, "socket reponse - success", function(){
								consoleLogger("Alpha Message sent to " + lastUser + " in room " + roomName);
							});
						}
					});
			}
		}

		// Get the room object. This approach should still work if connection has disconnected
		connectionObj.getApp().room(roomName, function(err, roomObj){
			if (err) { 
				logger(err);
				next(err);
				return;
			}
			// Returns an array of easyrtcid's in the room
			roomObj.getConnections(function(err, connectedEasyrtcidArray){

				if (err) { 
					logger(err);
					next(err);
					return;
				}		

				if (connectedEasyrtcidArray) {
					consoleLogger("Room ["+roomName+"] has "+connectedEasyrtcidArray.length+" clients in it", connectedEasyrtcidArray);
					if (connectedEasyrtcidArray.length === 0){
						
						if (existingRooms[roomName]) {
							delete existingRooms[roomName];
						}
						
						var logMessage = "Everyone Left. Terminating " + roomName;
						consoleLogger(logMessage);
						logger(logMessage);						
					}
				}
			});
			
		});
		next(null);
	}; // afterRoomLeave ends

	// Run the default roomLeave event handler, but with 'afterRoomLeave' next callback.
	easyrtc.events.emitDefault("roomLeave", connectionObj, roomName, afterRoomLeave);
};
easyrtc.events.on("roomLeave", customListenerForRoomLeave);	
// Room Leave End





































// Receive a message Start		
var customListenerForOnEasyrtcCmd = function(connectionObj, msg, socketCallback, next){

//	console.log("");
//	console.log("easyrtcCmd:",msg.msgType);
	easyrtc.events.emitDefault("easyrtcCmd", connectionObj, msg, socketCallback, next);	
	
};
easyrtc.events.on("easyrtcCmd", customListenerForOnEasyrtcCmd);		
// Receive a message End

























// Receive a message Start		
var customListenerForOnEasyrtcMsg = function(connectionObj, msg, socketCallback, next){


	var easyrtcid = connectionObj.getEasyrtcid();
	
	switch(msg.msgType){	

		case 'lockRoom':
			
			existingRooms[msg.msgData.roomName].isLocked=true;
			
			socketCallback({msgType:'lockRoomACK', msgData:''});
			
			var logMessage = 'Locked room: ' + msg.msgData.roomName;
			consoleLogger(logMessage);
			logger(logMessage);					
			break;
			

		case 'unlockRoom':
			
			existingRooms[msg.msgData.roomName].isLocked=false;
			socketCallback({msgType:'unlockRoomACK', msgData:''});

			var logMessage = 'Unlocked room: ' + msg.msgData.roomName;
			consoleLogger(logMessage);
			logger(logMessage);					
			break;
			
			

		case 'enablePublicListing':
			
			existingRooms[msg.msgData.roomName].isPublic=true;			
			socketCallback({msgType:'enablePublicListingACK', msgData:''});

			var logMessage = 'Enabled public listing for : ' + msg.msgData.roomName;
			consoleLogger(logMessage);
			logger(logMessage);				
			break;
			

		case 'disablePublicListing':
			
			existingRooms[msg.msgData.roomName].isPublic=false;						
			socketCallback({msgType:'disablePublicListingACK', msgData:''});
			
			var logMessage = 'Disabled public listing for : ' + msg.msgData.roomName;
			consoleLogger(logMessage);
			logger(logMessage);				
			break;
			

		case 'roomJoinRequest':
			
			var roomName = msg.msgData.roomName;
			var joinMessage = "Response";	
			consoleLogger("roomJoinRequestReceived from " + easyrtcid);
			
			if(existingRooms[roomName]){
				consoleLogger(roomName  + " exists");
			
				if(existingRooms[roomName].isLocked){
			
					socketCallback({msgType:'roomLocked', msgData:joinMessage});
					consoleLogger("Message sent to " + easyrtcid + " in room " + roomName + " telling user that room is locked");
				
				} else if(existingRooms[roomName].numUsers==maxUsers){
			
					socketCallback({msgType:'roomFull', msgData:joinMessage});
					consoleLogger("Message sent to " + easyrtcid + " in room " + roomName + "telling him that room is full");
				
				} else {
			
					socketCallback({msgType:'joinRoomPermit', msgData:"normalUser"});
					consoleLogger("Message [joinRoomPermit] [normalUser] sent to " + easyrtcid + " in room " + roomName + " with " + existingRooms[roomName].numUsers + " users");
					//to do stuff
				}
			
			} else {
			
//				consoleLogger("roomJoinRequestReceived from initiator : " + easyrtcid);				
				socketCallback({msgType:'joinRoomPermit', msgData:"adminUser"});					
				consoleLogger("Message [joinRoomPermit][adminUser] sent to " + easyrtcid + " trying to create room " + roomName);
			}
			next(null);
			break;			
		
		default:
			easyrtc.events.emitDefault("easyrtcMsg", connectionObj, msg, socketCallback, next);	
			break;
	} // end switch case
	
};
easyrtc.events.on("easyrtcMsg", customListenerForOnEasyrtcMsg);		
// Receive a message End


easyrtc.events.on("msgTypeGetRoomList", function(connectionObj, socketCallback, next){
    socketCallback(connectionObj.util.getErrorMsg("MSG_REJECT_NO_ROOM_LIST"));
});


//************* App Queries ************* 

httpApp.post("/postFeedback", function(request, response){
	
		var userIP = request.connection.remoteAddress;
		var body = '';
        request.on('data', function (data) {
            body += data;
			response.writeHead(200, {'Content-Type': 'text/html', 'Access-Control-Allow-Origin' : '*'});
	        response.end('Thank you for your submission.');			

        });

        request.on('end', function () {
            var POST = qs.parse(body);
			consoleLogger(POST);
			emailLogger(JSON.stringify(POST));

			var preTrim = POST.message;
			var postTrim = replaceAll(preTrim,"\r\n","<br>");
			
			html = "<b>IP:</b> " + userIP + "<br><b>Name:</b> " + POST.name + " <br>" + "<b>Email:</b> " + POST.email + "<br>" + "<b>Message:</b> " + postTrim;
			// setup e-mail data with unicode symbols
			var mailOptions = {
				from: "Dart Feedback<dartsessions@gmail.com>", // sender address
				to: "dartsessions@gmail.com", // list of receivers
				subject: "DartSessions: Feedback", // Subject line
				text: html, // plaintext body
				html: html // html body
			}			
			// send mail with defined transport object
			smtpTransport.sendMail(mailOptions, function(error, response){
				if(error){
					consoleLogger(error);
					logger(error)
					
				}else{
					consoleLogger("Message sent: " + response.message);
				}			
			});
			
			
			
        });
});


httpApp.get("/checkRoom/:roomName", function(request, response){
	
	var roomName= request.params.roomName;
	
	var result = false;
	
	if(existingRooms[roomName])result=true;
	
	var final = '{ "roomExists" : "' + result +'" }';

	response.writeHead(200, {	'content-type':'application/json',
								'Access-Control-Allow-Origin' : '*'}); 
	response.write(final);
	
	response.end();	

});


httpApp.get("/getRoomList", function(request, response){

	var finalText = "";
	var returnMap = {};
	for (var i in existingRooms){
		
		if (existingRooms[i].isPublic && !existingRooms[i].isLocked ){
				returnMap[i] = existingRooms[i].numUsers;
		}
	}	
	var final = JSON.stringify(returnMap);
	response.writeHead(200, { 'content-type':'application/json', 'Access-Control-Allow-Origin' : '*'});
	response.write(final);
	response.end();	


});


httpApp.get("/shutdown_sneaky", function(request, response){

	easyrtc.events.emit("shutdown");

	response.writeHead(200, { 'content-type':'text/html', 'Access-Control-Allow-Origin' : '*'});
	response.write("Shutdown");
	response.end();	


});


httpApp.get("/getRoomList_all_sneaky", function(request, response){

	var finalText = "";
	var returnMap = {};

	for (var i in existingRooms){

				returnMap[i] = existingRooms[i].numUsers;

	}	
	var final = JSON.stringify(returnMap);
	response.writeHead(200, { 'content-type':'application/json', 'Access-Control-Allow-Origin' : '*'});
	response.write(final);
	response.end();	


});


httpApp.get("/getRandomRoom", function(request, response){

	function getRandomRoomName(mycallback) {
	
		var part1 = 
		["lacking", "earthquake", "sort", "abounding", "elfin", "bell", "faithful", "wobble", "lopsided", "string", "bead", "listen", "argument", "ladybug", "deliver", "gate", "rhythm", "steer", "uneven", "grate", "loving", "scandalous", "uttermost", "marble", "railway", "rhyme", "camp", "use", "alert", "develop", "spray", "boy", "equal", "station", "sky", "hop", "kitty", "loose", "outstanding", "scarf", "natural", "discover", "merciful", "tax", "stove", "pin", "things", "stay", "memorise", "lazy", "weak", "roof", "birth", "simple", "abstracted", "tiny", "stir", "sound", "chew", "trip", "rural", "husky", "deserve", "value", "challenge", "need", "title", "strange", "achiever", "bitter", "zinc", "rotten", "carve", "coach", "queen", "obese", "thin", "belong", "lyrical", "skillful", "foregoing", "health", "afterthought", "bath", "appliance", "dress", "summer", "shaky", "strip", "apologise", "creepy", "truculent", "yellow", "tug", "blushing", "standing", "judge", "crown"];
		
		var part2 = 
		["jumpy", "faulty", "tap", "fabulous", "space", "absent", "cheat", "strong", "chase", "wool", "loud", "correct", "religion", "invention", "concentrate", "cemetery", "boil", "ritzy", "impolite", "quaint", "observant", "start", "acoustics", "thought", "raise", "theory", "incandescent", "magic", "war", "shock", "icky", "trip", "aromatic", "divergent", "ocean", "waste", "puncture", "trucks", "murky", "motionless", "doctor", "church", "poised", "boorish", "cellar", "mailbox", "wacky", "clean", "temper", "confused", "living", "tender", "lick", "land", "knit", "cause", "frantic", "pan", "impress", "steep", "fang", "aboriginal", "squirrel", "grain", "wind", "bikes", "shade", "apathetic", "bone", "sable", "warm", "pointless", "economic", "wet", "flashy", "itch", "tidy", "like", "moldy", "peace", "wonderful", "aftermath", "dear", "cruel", "honey", "dance", "rot", "bless", "null", "oafish", "occur", "romantic", "heavenly", "bawdy", "pedal", "flimsy", "equable", "mundane", "horn", "tip"];
	
		var randomNumber1 = Math.floor((Math.random()*99)+1);
		var randomNumber2 = Math.floor((Math.random()*99)+1);		
		var randomNumber3 = Math.floor((Math.random()*9)+1);


		
		var keepTrying = function(){ 
			var result = part1[randomNumber1] + '-' + part2[randomNumber2] + '-' + randomNumber3;		

			for (var i in existingRooms){
				if (existingRooms[i] == result){
					return keepTrying();
				}
			}			
			
			return result;
		};
		
		mycallback(keepTrying());

	}	
			
	getRandomRoomName(function(result){
		response.writeHead(200, { 'content-type':'application/json', 'Access-Control-Allow-Origin' : '*'}); 
		var final = '{ "roomName" : "' + result +'" }';
		response.write(final);
		response.end();			
	});	
});



httpApp.get("/:roomName", function(request, response){

});


httpApp.get("/", function(request, response){
	
	httpApp.configure(function() {
		httpApp.use(express.static(__dirname + "/static/start"));
	});
	
	response.sendfile(__dirname + '/static/start/index.html');
});

//************* End App Queries ************* 


// Start EasyRTC server
var rtc = easyrtc.listen(httpApp, socketServer);


